const mix = require('laravel-mix');

mix.webpackConfig({
    stats: {
        children: true,
    },
});

mix.js('resources/js/app.js', 'public/js')
    .react()
    .postCss("resources/css/app.css", "public/css", [
        require("tailwindcss"),
    ]);

mix.browserSync({
    browser: 'vivaldi',
    proxy: '127.0.0.1:8000'
});
