const defaultTheme = require('tailwindcss/defaultTheme');

module.exports = {
    content: [
        "./resources/**/*.blade.php",
        "./resources/**/*.js",
        "./resources/**/*.vue",
    ],
    important: "#root",
    theme: {
        fontFamily: {
            'sans' : [ 'Nunito Sans', defaultTheme.fontFamily.sans ],
        },
        extend: {},
    },
    plugins: [],
}
