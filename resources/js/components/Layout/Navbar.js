import { IconButton } from '@mui/material';
import AddShoppingCartIcon from '@mui/icons-material/AddShoppingCart';
import imagePerson from '../Image/Valen.png';
import React from 'react';

class Logo extends React.Component {
    render() {
        return (
            <div className="text-2xl text-center md:text-left text-gray-800 font-bold hover:text-gray-800 md:p-0 p-2 mt-2 md:mt-0 ">
                <a href="/">Anomaly</a>
            </div>
        );
    };
}

class Cart extends React.Component {
    render() {

        return (
            <div className="">
                <IconButton color="primary" aria-label="add to shopping cart">
                    <AddShoppingCartIcon />
                </IconButton>
            </div>
        );
    };
}

class HamMenu extends React.Component {
    render() {
        return (
            <div>
                <svg className="h-6 w-6 fill-current" viewBox="0 0 24 24">
                    <path d="M4 5h16a1 1 0 0 1 0 2H4a1 1 0 1 1 0-2zm0 6h16a1 1 0 0 1 0 2H4a1 1 0 0 1 0-2zm0 6h16a1 1 0 0 1 0 2H4a1 1 0 0 1 0-2z" />
                </svg>
            </div>
        );
    };
}

class Search extends React.Component {
    render() {
        const hidden = 'hidden md:inline-block ';
        const padding = 'px-4 py-3 mx-4 ';
        const searchBg = 'text-sm rounded focus:outline-none focus:shadow-outline ';
        const searchBgColor = 'text-gray-400 bg-gray-900 placeholder-gray-200 ';
        const searchBar = hidden + searchBg + searchBgColor + padding;
        return (
            <div>
                <input
                    type="text"
                    className={searchBar}
                    placeholder="Search" />
            </div>
        );
    };
}


class NavOpen extends React.Component {
    render() {

        const navBg = 'md:hidden py-2 px-2 transition ease-in-out text-gray-800 rounded hover:bg-gray-900 hover:text-gray-100 hover:font-medium md:mx-2';
        return (
            <div className="md:hidden w-full pb-2 md:flex md:items-center md:justify-between md:pb-0">
                <div className="flex flex-col px-2 md:flex-row">
                    <a href="/" className={navBg}>Home</a>
                    <a href="/about" className={navBg}>About</a>
                    <a href="/contact" className={navBg}>Contact</a>
                </div>
                <div className="transition ease-in-out flex item-center">
                    <Search />
                    <div className="mt-4 inline-block flex mx-auto">
                        <a href="/signin"> <button className="inline-block text-black bg-transparent px-4 md:px-2 md:w-24 py-2 lg:w-36 rounded-md sm:w-64 w-48"> Sign In </button> </a>
                        <a href="/signup"> <button className="inline-block text-white bg-indigo-800 px-4 md:px-2 md:w-24 py-2 lg:w-36 rounded-md sm:w-64 w-48"> Sign Up </button> </a>
                    </div>
                </div>
            </div>
        );
    };
}

function NavLists() {

        const [signedIn, setSignedIn] = React.useState(false);
        const showSignedIn = () => {
            if (!signedIn) {
                setSignedIn(true);
            } else {
                setSignedIn(false);
            }
        };


        const ava = <img onClick={showSignedIn} class="w-11 h-11 ml-4 rounded-md md:inline-block hidden" src={ imagePerson } alt="Rounded avatar" />
        const sign = <div>

                        <a href="/signin">
                            <button
                                onClick={showSignedIn}
                                className="hidden md:inline-block text-black bg-transparent px-4 md:px-2 md:w-24 py-2 lg:w-36 rounded-md sm:w-64 w-48">
                                Sign In
                            </button>
                        </a>

                        <a href="/signup">
                            <button
                                className="hidden md:inline-block text-white bg-indigo-800 px-4 md:px-2 md:w-24 py-2 lg:w-36 rounded-md sm:w-64 w-48">
                                Sign Up
                            </button>
                        </a>

                     </div>
        const navBg = 'hidden md:inline-block py-2 px-2 text-gray-800 rounded hover:bg-gray-900 hover:text-gray-100 hover:font-medium md:mx-2';

        return (
            <div className="w-full pb-2 md:flex md:items-center md:justify-between md:pb-0">
                <div className="flex flex-col px-2 md:flex-row">
                    <a href="/" className={navBg}>Home</a>
                    <a href="/about" className={navBg}>About</a>
                    <a href="/contact" className={navBg}>Contact</a>
                </div>
                <div className="flex item-center">
                    <Search />
                    <div className="hidden sm:inline-block flex">
                        {
                            signedIn ?
                            sign :
                                <div className="container grid grid-cols-2">
                                <div className="h-full md:inline-block hidden  items-center">
                                    <p className="mt-[2px] text-[12px] font-bold">
                                        CARAKA
                                    </p>
                                    <p className="text-[12px]">
                                        <i> Member </i>
                                    </p>
                                </div>
                                { ava }
                            </div>
                        }

                    </div>
                </div>
            </div>
        );
}


export default function Navbar() {

    const [show, setShow] = React.useState(false);
    const showNavLists = () => {
        if (!show) {
            setShow(true);
        } else {
            setShow(false);
        }
    };

    const buttonBg = "p-2 rounded-md shadow-md block active:bg-gray-300 text-gray-900 hover:bg-gray-200 hover:text-gray-700 focus:text-gray-700 focus:outline-none";
    const ava = <img class="w-11 h-11 ml-4 rounded-md inline-block md:hidden" src={ imagePerson } alt="Rounded avatar" />

    return (
        <header >
            <div className="relative z-50">
                <nav className="fixed w-screen top-0 px-6 py-2 bg-white shadow-md md:flex shadow-md">
                    <div className="flex justify-between items-center mx-auto">
                        <div className="flex">
                            <div className="md:hidden">
                                <button type="button" className={buttonBg} onClick={showNavLists} >
                                    <HamMenu />
                                </button>
                            </div>
                        </div>


                        <Logo />
                        <div>
                            {ava}
                        </div>


                    </div>

                    {show ? <NavOpen /> : null}
                    <NavLists />

                </nav>
            </div>

            <div className="h-24 md:h-16" />
        </header>
    );
}
