import React from 'react';
import { Typography } from '@mui/material';

class Title extends React.Component {
    render() {
        return (
            <div className="font-bold text-4xl text-left md:text-center">
                <Typography variant="root">{this.props.name}</Typography>
            </div>
        )
    }
}
export default Title;
