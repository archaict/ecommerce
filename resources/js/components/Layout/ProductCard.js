import { CardContent, CardMedia, Typography } from '@mui/material';
import Card from '@mui/material/Card';

import image from '../Image/product.jpg';
import React from 'react';

import Cart from './Asset/Cart'

class ProductCard extends React.Component {

    render() {
        const cardBg = 'hover:scale-105 min-w-[100px] mx-auto mt-16 hover:bg-[#eeeeee] bg-[#fafafa] shadow-md transition ease-in-out delay-150 items-end justify-end';
        return (
            <div>
                <Card className={cardBg} >

                    <a href="/producLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Egestas egestas fringilla phasellus faucibus scelerisque eleifend donec pretium vulputate. Feugiat in fermentum posuere urna. Amet nisl purus in mollis nunc. Tellus orci ac auctor augue mauris augue. Dolor morbi non arcu risus quis varius quam quisque id. Et malesuada fames ac turpis egestas sed tempus. Eget mi proin sed libero enim sed faucibus. Turpis massa sed elementum tempus. Congue eu consequat ac felis donec.t">
                        <CardMedia
                            component="img"
                            /* image={this.props.image} */
                            image={image}
                        />
                    </a>

                    <Cart />

                    <CardContent >

                        <div>
                            <Typography variant="body2" color="text.primary" className='font-semibold'>
                                {this.props.productName}
                            </Typography>

                            <Typography variant="body2" color="text.secondary">
                                {this.props.productDescription}
                            </Typography>
                        </div>

                        <Typography className="text-md font-semibold py-2">
                            Rp20.000,-
                        </Typography>

                    </CardContent>

                </Card>
            </div>
        );
    }
}


export default ProductCard;
