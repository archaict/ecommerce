import React from 'react';
import { Typography } from '@mui/material';

class Subtitle extends React.Component {
    render() {
        return (
            <div className="mt-2 text-left md:w-full md:text-center"> <Typography variant="subtitle"> <i>{this.props.sub}</i> </Typography> </div>
        )
    }
}

export default Subtitle;
