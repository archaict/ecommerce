import React from 'react';
import Title from './Title';
import Subtitle from './Subtitle';

class Sections extends React.Component {
    render() {
        return (
            <div className="container md:mx-auto mx-9 mt-[3rem]" >
                <Title name={this.props.name} />
                <Subtitle sub={this.props.sub} />
            </div>
        )
    }
}

export default Sections;
