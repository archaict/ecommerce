import React from 'react';

export default class Blank extends React.Component {
    render () {
        return (
            <div className="text-center mt-96 sm:mt-[400px]">
                <p className="font-bold text-4xl"> Blank Page </p>
                <div className="h-[1px] my-1 bg-gray-400 w-48 mx-auto" />
                <p className="text-lg font-medium"> <i> Nothing to see here </i> </p>
            </div>
        );
    }
}
