import React from 'react';

export default class ButtonWithLink extends React.Component {
    render() {
        const buttonBg = "inline-block w-36 px-6 py-2 bg-red-700 text-white font-semibold text-sm leading-snug uppercase rounded shadow-md hover:bg-red-800 hover:shadow-lg focus:bg-red-800 focus:shadow-lg focus:outline-none focus:ring-0 active:bg-red-800 active:shadow-lg transition duration-150 ease-in-out w-full my-4";
        const links = this.props.link ? this.props.link : "/";
        return(
            <a href={links}>
                <button className={buttonBg}>
                    {this.props.name}
                </button>
            </a>
        );
    };
}
