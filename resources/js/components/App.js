import { BrowserRouter, Routes, Route } from "react-router-dom";

import React from 'react';
import ReactDOM from 'react-dom';

import About from "./Page/About";
import BlankPage from './Page/BlankPage';
import Cart from './Page/Cart';
import ComingSoon from './Page/ComingSoon';
import Home from "./Page/Home";
import Product from './Page/Product';
import SignIn from './Page/SignIn';
import SignUp from './Page/SignUp';
import Thankyou from './Page/Thankyou';

function App() {
    return (
        <Routes>

            <Route path="/" element={<Home /> } />

            <Route path="/about" element={<About /> } />
            <Route path="/product" element={<Product /> } />

            {/*Important*/}
            <Route path="/contact" element={<BlankPage /> } />


            <Route path="/cart" element={<Cart /> } />

            <Route path="/signin" element={<SignIn /> } />
            <Route path="/signup" element={<SignUp /> } />

            <Route path="/thankyou" element={ <Thankyou />} />
            <Route path="/wishlist" element={ <ComingSoon link="/product" />} />

            <Route path="/forgot-password" element={<SignIn /> } />

        </Routes>
    );
}
export default App;

if (document.getElementById('root')) {
    ReactDOM.render(
        <BrowserRouter>
            <App />
        </BrowserRouter>,
        document.getElementById('root'));
}

// Tampilan menu di kiri
// 60 gambar 40 form
// kalau belum punya akun register
