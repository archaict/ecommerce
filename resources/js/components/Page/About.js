import React from 'react';

import Footer from '../Layout/Footer';
import Navbar from '../Layout/Navbar';

function About() {
    return (
        <div className="bg-gray-100">
            <Navbar />
            <div className="text-center mt-96 sm:mt-[220px]">
                <p className="font-bold text-4xl"> About Us </p>

                <div className="h-[1px] my-1 bg-gray-400 w-48 mx-auto" />
                <p className="text-lg font-medium w-[60%] text-center mx-auto mt-12">
                    <i>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                        Sed scelerisque vehicula justo, at sagittis erat auctor at.
                        Donec leo ipsum, sodales id accumsan et, cursus ut erat.
                        Aliquam sit amet dolor sit amet elit laoreet posuere.
                        Sed semper dui vel tortor rhoncus bibendum.
                        Duis tortor turpis, laoreet quis leo vel, ornare consequat lacus.
                        Nam tincidunt, lorem ac commodo dapibus, diam arcu vulputate massa, a cursus turpis justo at orci.
                        Fusce risus lectus, pulvinar ut pretium vitae, tincidunt ac ligula.
                        Duis a nulla eget enim elementum ultrices.
                        Vestibulum consequat leo nec nisi scelerisque, et convallis nisl posuere.
                        Nullam sed semper mauris, eget euismod lacus.
                        Maecenas eget rhoncus leo, fermentum posuere diam.
                        Suspendisse sit amet lectus odio.
                    </i>
                </p>
                <p className="text-lg font-medium w-[60%] text-center mx-auto mt-12">
                    <i>
                        Maecenas condimentum sapien quis augue rutrum, ut faucibus mi sollicitudin.
                        Quisque sit amet sollicitudin libero, at ultrices nisi.
                        Phasellus a ipsum eu purus sodales blandit. Vivamus imperdiet massa ac congue blandit.
                        Maecenas nisl dui, rhoncus nec nibh in, auctor mollis eros.
                        Maecenas a felis a quam bibendum semper.
                        Maecenas ullamcorper dolor risus, id dignissim orci cursus non.
                        Proin id libero felis.
                        Vivamus varius leo nulla, non eleifend odio laoreet a.
                        Mauris aliquam fringilla urna nec aliquet.
                        Pellentesque eget tincidunt sem, in venenatis orci.
                        In vestibulum neque sit amet lorem ullamcorper posuere.
                        Suspendisse potenti.
                        Suspendisse efficitur, dolor et suscipit congue, augue nisi tincidunt odio,
                        ut sodales dui quam vitae lorem.
                    </i>
                </p>
            </div>
            <Footer />
        </div >
    );
}

export default About;
