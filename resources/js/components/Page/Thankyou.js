import React from 'react';
import image from '../Image/thankyou.png';

export default function Thankyou() {
    return (
                <div className="mt-40 sm:mt-[260px] text-center text-2xl sm:text-3xl">
                    <img className="mx-auto w-[300px]" src={image} />
                    <p> Thank you for your purchases! </p>
                    <a href="/">
                        <button className="text-sm mt-6 px-6 py-2 shadow-md rounded-sm bg-gray-800 text-white">
                            Go Back
                        </button>
                    </a>
                </div>
    );
}
