import { TextField } from '@mui/material';

import React from 'react';

class SignInHead extends React.Component {

    constructor(props) {
        super(props);

    }

    render = () => {
        return <div className="text-center">
                <p className="font-bold text-2xl"> Sign In Page </p>
                <p className="text-sm"> <i>Please complete your registration!</i> </p>
            </div>;
    }
}

export default function SignIn() {
    return (

        <div className="flex h-screen">
            <div className="flex sm:w-[60%] bg-gradient-to-r from-purple-500 to-pink-500">
            </div>

            <div className="flex sm:w-[40%] h-screen w-full bg-slate-100">
                <div class="w-4/6 m-auto">

                    <SignInHead />

                    <div className="w-[90%] mx-auto">

                        <div class="my-6"> <TextField label="Name *" color="grey" fullWidth /> </div>
                        <div class="my-6"> <TextField label="Password *" color="grey" fullWidth /> </div>

                        <a href="/">
                            <button className="inline-block px-7 py-3 bg-red-700 text-white font-semibold text-sm leading-snug uppercase rounded shadow-md hover:bg-red-800 hover:shadow-lg focus:bg-red-800 focus:shadow-lg focus:outline-none focus:ring-0 active:bg-red-800 active:shadow-lg transition duration-150 ease-in-out w-full" >
                                Sign In
                            </button>
                        </a>

                    </div>



                </div>
            </div>

        </div>


        // <div class="h-screen bg-gray-100">
        // </div>
    );
}
