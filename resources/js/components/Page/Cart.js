import { TextField } from '@mui/material';

import React from 'react';

import Footer from '../Layout/Footer';
import Navbar from '../Layout/Navbar';
import image from '../Image/cardTest.jpg'

class CartItem extends React.Component {

    render () {
        return (

            <div className="">
                <div className="flex grid-cols-2 grid">
                    <div className="flex">
                        <img src={image} className='rounded-full w-14 h-14'/>
                        <div className ="ml-6 mt-[4px]">
                            <p className="font-semibold"> Anomaly Product #1 </p>
                            <p className="text-gray-500"> #123456 </p>
                        </div>
                    </div>
                    <a href="/cart" className="text-right font-bold w-[80%] mt-4"> x </a>
                </div>
                <div className="w-11/12 bg-gray-300 my-4 h-[1px]" />
            </div>
        );
    }

}

export default function Cart () {

    return (
        <div className="h-screen bg-gray-100">

            <Navbar />

            <div className="container mt-10 md:mt-48 flex inline-block flex-wrap sm:flex-nowrap gaps-8 mx-auto">


                <div className="w-full sm:w-2/3">
                    <p className="font-bold ml-4 sm:ml-8 mb-12 text-2xl">
                        Shopping Cart
                    </p>


                    <div className="ml-6 mb-10 sm:mb-0">

                        <CartItem />
                        <CartItem />
                        <CartItem />
                        <CartItem />

                    </div>

                </div>



                <div className="mx-auto md:ml-20 w-11/12 md:w-1/3">
                    <p className="text-2xl ml-0 font-bold mb-8">
                        Card Details
                    </p>

                    <div className="mx-auto grid grid-cols-2">
                        <TextField label="Full Name" variant="standard" className="my-2 mx-2" />
                        <TextField label="Card Number" variant="standard" className="my-2 mx-2" />

                        <TextField label="Address" variant="standard" className="my-2 mx-2" />
                        <TextField label="Expiration Date" variant="standard" className="my-2 mx-2" />
                        <TextField label="CVV" variant="standard" className="my-2 mx-2" />

                        <a href="/thankyou" className="col-span-2">
                            <button className="w-full mt-10 sm:mt-20 rounded-md shadow-md px-6 py-2 bg-blue-600 text-white">
                                Check Out
                            </button>
                        </a>
                    </div>

                </div>


            </div>
            <Footer />

        </div >
    );
}
