import { Typography } from '@mui/material';

import React from 'react';

import Footer from '../Layout/Footer';
import Navbar from '../Layout/Navbar';
import image from '../Image/productpage.jpeg';

class Spacer extends React.Component {
    render() {
        return (
            <div className="mb-8 lg:mb-12" />
        );
    }
}

class LeftSide extends React.Component {
    render() {
        return (
            <div className="sm:w-full md:w-10/12 xl:w-3/4 mb-12 md:mb-0 mx-auto"> {/*Left*/}
                <img src={image} class="sm:min-w-[320px] max-w-full h-100" alt="..." />
            </div>
        );
    };
}

class RightSide extends React.Component {
    render() {
        return (

            <div className="sm:w-full md:w-10/12 xl:w-3/4 mb-24 md:mb-0">{/*Right*/}

                <p className="sm:inline-block hidden"> Category </p>
                <p className="text-4xl font-bold "> Anomaly Product </p>

                <Spacer />

                <div className="flex mx-auto w-full">
                    <p className="text-sm font-bold mr-1"> Rp </p>
                    <p className="text-2xl font-bold"> {this.props.price},- </p>
                </div>

                  <Spacer />

                <Typography className="mx-auto md:mx-0 mb-12 text-justify md:text-left sm:max-w-2/3">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                    sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                    Egestas egestas fringilla phasellus faucibus scelerisque eleifend
                    donec pretium vulputate. Feugiat in fermentum posuere urna.
                    Amet nisl purus in mollis nunc.
                </Typography>

                {/* <ButtonLink name="Add to Cart" link="/cart"/> */}

                <div className="flex w-full justify-center items-center md:justify-start">

                    <a href="/cart">
                        <button className="w-36 bg-red-700 shadow-md mx-4 md:mx-0 rounded-md text-white font-semibold px-6 py-2 hover:bg-red-800 active:bg-red-800 active:shadow-md transition ease-in-out">
                            Add To Cart
                        </button>
                    </a>

                    <a href="/">
                        <button className="w-36 shadow-md rounded-md mx-4 md:ml-8 font-semibold px-6 py-2 hover:bg-gray-200 active:bg-gray-200 active:shadow-md transition ease-in-out">
                            Go Back
                        </button>
                    </a>


                </div>



                <div className="container">
                    <a href="/wishlist">
                        <p className="text-center md:text-left font-bold mt-12">
                            Add to Wishlist
                        </p>
                    </a>
                    <div className="mx-auto md:mx-0 w-[103px] bg-gray-600 h-[1px]" />
                </div>

            </div>

        );
    };
}

export default function Product() {
    return (
        <div className="bg-gray-100 ">
            <Navbar />

            <div>
                <div className="mt-[-20px] md:mt-48">

                    <div className="w-[90%] md:w-full lg:container px-6 py-12 mx-auto ">
                        <div className="grid grid-cols-1 md:grid-cols-2 justify-center items-center text-center md:text-left">

                            <LeftSide />
                            <RightSide price="199.000" />

                        </div>
                    </div>

                </div>
            </div>
        </div >
    );
}
