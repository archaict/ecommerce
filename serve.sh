#!/usr/bin/env bash

trap 'exit' INT TERM
trap 'kill 0' EXIT

npm run watch &
php artisan serve &

for job in $(jobs -p); do
    wait $job
done
